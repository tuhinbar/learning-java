import java.util.Scanner;

public class JavaAssignment5 {
    public static int printFactorial(int n){
        if(n==1||n==0){
            return 1;
        }
      int fact_nm1=  printFactorial(n-1);
      int fact_n=n*fact_nm1;
      return fact_n;
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a number:");
        int ans=printFactorial(sc.nextInt());
        System.out.println(ans);

    }
}
