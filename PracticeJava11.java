import java.util.ArrayList;
import java.util.Collections;

public class PracticeJava11 {
    public static void main(String[] args) {
        ArrayList<String> car = new ArrayList<String>();
        car.add("Honda");
        car.add("Suzuki");
        car.add("Alto");
        car.add("BMW");
        System.out.println(car);
        //swapping two elements in an arraylist
        ArrayList<Integer> c1 = new ArrayList<>();
        c1.add(12);
        c1.add(13);
        c1.add(14);
        c1.add(23);
        c1.add(16);
        c1.add(17);
        for (Integer c : c1) {
            System.out.println(c);
        }
        Collections.swap(c1, 2, 4);
        System.out.println("ArrayList after swap:" + c1);
        // for(Integer b:c1){
        //System.out.println(c1)
        ArrayList<Integer>c2=new ArrayList<>();
        c2.add(56);
        c2.add(13);
        c2.add(14);
        c2.add(15);
        c2.add(16);
        c2.add(10);
        ArrayList<String> c3=new ArrayList<>();
        for(Integer e:c1)
            c3.add(c2.contains(e) ? "yes":"no");
            System.out.println(c3);
        }
    }
