class Vehicle {

    String name = "Suzuki";
}

class Car extends Vehicle {

    String name = "Honda";

    public void printMyName() {

        System.out.println(name);
    }

    public void printParentName() {
        System.out.println(super.name);
    }

    public static void main(String[] args) {

        Car myCar = new Car();
        System.out.print("My Car's Name: ");
        myCar.printMyName();
        System.out.print("My Parent Vehicle's Name: ");
        myCar.printParentName();
    }
}