
 class Demo
{
    int x = 100;
}
 class Sub extends Demo
{
    int x = 200;
    void display()
    {

        System.out.println("Value of variable of Sub: " +x);
        System.out.println("Value of variable of Demo: " +super.x);
    }
    public static void main(String[] args)
    {
        Sub s = new Sub();
        s.display();
    }
}


