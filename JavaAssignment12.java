//swapping two variables.
import java.util.Scanner;

public class JavaAssignment12 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter two numbers that you want to swap:");
        int a=sc.nextInt();
        int b=sc.nextInt();
        System.out.println("Before swapping the numbers are :"+(a)+","+ (b));
        a=a+b;
        b=a-b;
        a=a-b;
        System.out.println("After swapping the numbers:"+(a)+","+(b));

    }
}
