public class JavaAssignment3 {
    public static void printNumb(int n){
        //base case
        if(n==6) {
            return;
        }
        System.out.println(n);
        printNumb(n+1);
    }
    public static void main(String[]args){
        //recursion
        int n=1;
        printNumb(n);

    }
}
