import java.util.*;
public class LearnJavaCollectionClass {
    public static void main(String[] args) {
        List<Integer>list=new ArrayList<>();
        list.add(1);
        list.add(11);
        list.add(45);
        list.add(45);
        list.add(11);
        list.add(45);
        list.add(44);
        list.add(22);
        list.add(16);
        System.out.println("minimum element"+" " +Collections.min(list));
        System.out.println("maximum element"+" " +Collections.max(list));
        System.out.println(Collections.frequency(list,45));
        Collections.sort(list);
        System.out.println(list);


    }
}
