import java.util.ArrayList;
import java.util.List;

class studentDatabase{
    String name;
    String section;
    int age;
    public studentDatabase(String name,String section,int age){
        this.name=name;
        this.section=section;
        this.age=age;
    }
}
public class PracticeJava19 {
    public static void main(String[] args) {
        List<studentDatabase> l=new ArrayList<>();
        studentDatabase s1=new studentDatabase("Aman Roy","A",18);
        studentDatabase s2=new studentDatabase("Ayan Mondal","A",18);
        studentDatabase s3=new studentDatabase("Bikash Das","B",18);
        studentDatabase s4=new studentDatabase("Anmol Kumar","D",16);
        studentDatabase s5=new studentDatabase("Payel Das","D",18);
        studentDatabase s6=new studentDatabase("Bipasha Pal","A",17);
        studentDatabase s7=new studentDatabase("Supratik Barman","B",18);
        l.add(s1);
        l.add(s2);
        l.add(s3);
        l.add(s4);
        l.add(s5);
        l.add(s6);
        l.add(s7);
        for(studentDatabase sd1:l){
            System.out.println("Name:"+sd1.name+" "+"Section:"+ " "+sd1.section+" "+"Age:"+sd1.age);
        }


    }
}