import javax.print.attribute.IntegerSyntax;
import java.util.*;

public class TestJavaCollection11 {
    //ArrayDeque
    public static void main(String[] args) {
        ArrayDeque<Integer>adq=new ArrayDeque<Integer>();
        adq.offer(21);
        adq.offer(23);
        adq.offerFirst(45);
        adq.offerLast(88);
        System.out.println(adq);
        System.out.println(adq.peek());
        System.out.println(adq.poll());
        System.out.println(adq);
        System.out.println(adq.pollFirst());
        System.out.println(adq.pollLast());
        //HashSet,LinkedHashSet,TreeSet.
       Set<Integer>set=new HashSet<>();
       set.add(11);
       set.add(33);
       set.add(34);
       set.add(36);
       set.add(77);
       set.add(48);
       set.add(22);
       set.add(36);
       set.remove(11);
        System.out.println(set);
        System.out.println();
        System.out.println(set.contains(49));
        Set<Integer>Student=new HashSet<Integer>();


        
    }
}
