public class LearnExceptionHandling7 {
    public static int Abc(){
        try {
            int a = 50;
            int b = 2;
            int c=a/b;
            return c;
        } catch (Exception e) {
            System.out.println(e);

        } finally {
            System.out.println("This is the end of the function.");
        }

        return 0;
    }
    public static void main(String[] args) {
        int i=Abc();
        System.out.println(i);

    }
}
