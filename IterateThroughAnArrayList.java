import java.util.ArrayList;

public class IterateThroughAnArrayList {
    public static void main(String[] args) {
        ArrayList<String>Animals=new ArrayList<>();
        Animals.add("Cat");
        Animals.add("Dog");
        Animals.add("Cow"); 
        System.out.println("ArrayList:"+Animals);
        System.out.println("Accessing individual elements:");
        for(String language:Animals){
            System.out.print(language);
            System.out.print(",");
        }






    }
}
