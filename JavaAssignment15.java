import java.util.Scanner;

public class JavaAssignment15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input an Integer:");
        Long n = sc.nextLong();
        System.out.println("The sum of the digits is :" + sumDigits(n));


    }

    public static int sumDigits(long n){
        int sum=0;
        while (n != 0) {
            sum+=n%10;
            n/=10;
        }
        return sum;
    }

}