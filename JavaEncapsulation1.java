class Area{
    int base;
    int height;
    Area (int base,int height){
        this.base=base;
        this.height=height;

    }
    public void getArea()
    {
        int area=base*height;
        System.out.println("Area of parallelogram is : "+ area);
    }
}

public class JavaEncapsulation1 {
    public static void main(String[] args) {
        Area parallelogram=new Area(3,5);
        parallelogram.getArea();
    }

}
