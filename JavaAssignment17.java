import java.util.Scanner;

public class JavaAssignment17 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter three numbers to compare :");
        double a=sc.nextDouble();
        double b=sc.nextDouble();
        double c=sc.nextDouble();
        System.out.println("The smallest number is"+" "+smallest(a,b,c));
    }
    public static double smallest(double a,double b,double c) {
        return Math.min(Math.min(a, b), c);
    }
    }

       