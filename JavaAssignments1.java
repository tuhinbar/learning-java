import java.util.Scanner;

//factorial of a number.
public class JavaAssignments1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a number:");
        double n= sc.nextDouble();
        int i,fact=1;
        for(i=1;i<=n;i++){
            fact=fact*i;
        }
        System.out.println("Factorial of "+ n +" is " + fact +".");
    }
}
