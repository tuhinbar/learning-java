import java.util.Arrays;
import java.util.Scanner;
public class AnagramTest {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Input two Strings:");
        String s1=sc.nextLine();
        String s2=sc.nextLine();
        char[]a1=s1.toCharArray();
        char[]a2=s2.toCharArray();
        Arrays.sort(a1);
        Arrays.sort(a2);
        boolean result=Arrays.equals(a1,a2);
        if(result)
            System.out.println("Strings are anagram.");
        else{
            System.out.println("Strings are not anagram.");
        }

    }
}
