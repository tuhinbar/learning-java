import java.util.ArrayList;
import java.util.List;

class Book2{
    String name;
    int value;
    String author;
    public Book2(String name,int value,String author){
        this.author=author;
        this.value=value;
        this.name=name;
    }

}
public class PracticeJava21 {
    public static void main(String[] args) {
        List<Book2> b1 = new ArrayList<>();
        Book2 book1 = new Book2("abc", 230, "abc");
        Book2 book2 = new Book2("abc", 340, "abc");
        Book2 book3 = new Book2("bcd", 670, "efg");
        Book2 book4 = new Book2("bcd", 560, "ghj");
        Book2 book5 = new Book2("jkl", 450, "uyt");
        b1.add(book1);
        b1.add(book2);
        b1.add(book3);
        b1.add(book4);
        b1.add(book5);
        System.out.println(b1);
        System.out.println("after iteration:");
        for(Book2 B2:b1){
            System.out.println("name:"+B2.name+"value:"+B2.value+"author:"+B2.author);
        }

    }

    }