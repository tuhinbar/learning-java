public class WrapperClass1 {
    public static void main(String[] args) {
       int a=5;
       double b=10;
       Integer aobj=Integer.valueOf(a);
       Double bobj=Double.valueOf(b);
       if (aobj instanceof Integer){
           System.out.println("An object of Integer is created.");}
       if(bobj instanceof  Double){
           System.out.println("An object of Double is created.");
       }

    }
}
