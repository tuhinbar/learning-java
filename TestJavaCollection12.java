import java.util.*;
class Student{
    String name;
    int rollno;

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", rollno=" + rollno +
                '}';
    }

    public Student(String name, int rollno){
        this.name=name;
        this.rollno=rollno;
    }

}




public class TestJavaCollection12 {
    public static void main(String[] args) {
        Set<Student>studentSet=new HashSet<>();
        studentSet.add(new Student("Nirmal Kar",11));
        studentSet.add(new Student("Anuj Kumar",12));
        studentSet.add(new Student("BIshaal Roy",13));
        studentSet.add(new Student("Aman Kumar",14));
        studentSet.add(new Student("Neeraj Kumar",15));
        System.out.println(studentSet);



    }
}
