import java.util.Scanner;

public class PracticeJava10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a number:");
        int n=sc.nextInt();
        System.out.println(test(n));
    }

        public static int test ( int n){
            int num = 10;
            while (n / num != 0) {
                num *= 10;
            }
            return Math.abs(n / (num / 10));
        }
    }

