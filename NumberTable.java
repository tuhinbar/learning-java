import java.util.Scanner;

public class NumberTable {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Please enter a number:");
        int n=sc.nextInt();
        for(int i=1;i<=20;i++){
            System.out.println(n*i);
        }
    }
}
