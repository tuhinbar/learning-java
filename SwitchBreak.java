import java.util.Scanner;

public class SwitchBreak {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("enter a button number:");
        int button =sc.nextInt();
        switch (button){
            case(1):
                System.out.println("Hello!");
                break;
            case(2):
                System.out.println("Namaste!");
                break;
            case(3):
                System.out.println("Bonjour!");
                break;
            case(4):
                System.out.println("Amigo!");
                break;
            default:
                System.out.println("Please enter a valid button.");
            }
        }

    }
