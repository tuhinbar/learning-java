public class WrapperObjectToPrimitiveType {
    public static void main(String[] args) {
        Integer abcObj=Integer.valueOf(23);
        Float cdeObj=Float.valueOf(String.valueOf(45.2));
        int abc = abcObj.intValue();
        double cde=cdeObj.intValue();
        System.out.println("the value of abc:"+" "+abc);
        System.out.println("the value of cde:"+" "+cde);



    }

}
