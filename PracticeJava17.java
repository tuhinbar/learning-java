import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class PracticeJava17 {
    public static void main(String[] args) {
        List<String> l1=new ArrayList<>();
        l1.add("SBI");
        l1.add("Axis");
        l1.add("ICIC");
        ListIterator<String>ltr=l1.listIterator();
        while(ltr.hasNext()){
            System.out.println("index:"+ltr.nextIndex()+ "name:"+ltr.next());
        }
        System.out.println("Traversing elements in backward manner:");
        while(ltr.hasPrevious()){
            System.out.println("index:"+ltr.previousIndex()+ "name:"+ltr.previous());
        }
    }
}
