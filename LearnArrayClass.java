import java.util.Arrays;

public class LearnArrayClass {
    public static void main(String[] args) {
        int[] numbers = {1, 2, 3, 4, 5, 6, 7};
        int index = Arrays.binarySearch(numbers, 4);
        System.out.println(index);
        int[] number = {2, 4, 5, 65, 87, 34, 99};
        Arrays.sort(number);
        for (int i : number) {
            System.out.println(i);
        }
    }
}