import java.util.Scanner;

public class LearnExceptionHandling5 {
    static void checkAge(int age){
        if(age<18) {
            throw new ArithmeticException("Access denied.");
        }else{
            System.out.println("Access granted.");
        }
    }

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Please enter a age:");
        int age=sc.nextInt();
        checkAge(age);
    }
}
