import java.util.*;
public class TestJavaCollection7 {
    public static void main(String[] args) {
        List<String>list1=new ArrayList<String>();
        list1.add("Portugal");
        list1.add("Argentina");
        list1.add("France");
        list1.add("Spain");
        Collections.sort(list1);
        for(String country:list1)
            System.out.println(country);


            System.out.println("Sorting numbers..");
            List<Integer>list2=new ArrayList<Integer>();
            list2.add(11);
            list2.add(22);
            list2.add(34);
            list2.add(55);
            list2.remove(1);
            Collections.sort(list2);
            for(Integer number:list2)
                System.out.println(number);


            
        }


    }
