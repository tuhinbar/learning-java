import java.util.*;
public class TestJavaCollection5 {
    //HashSet
    public static void main(String[] args) {
        HashSet<String>set=new HashSet<String>();
        set.add("Honda");
        set.add("Hyundai");
        set.add("Suzuki");
        set.add("Toyota");
        Iterator<String>itr=set.iterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }

    }
}
