import java.util.Scanner;

public class JavaAssignment10 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter first number:");
        int num1=sc.nextInt();
        System.out.println("Enter second number:");
        int num2=sc.nextInt();
        System.out.println("Enter third number:");
        int num3=sc.nextInt();
        double avg=num1+num2+num3/3.0;
        System.out.println("The average of the given numbers is:" + avg);

    }
}
