import java.util.Scanner;

public class SwitchBreak1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Enter a size :");
        int size=sc.nextInt();
        switch(size){
            case 29:
                System.out.println("Short");
                break;
            case 32:
                System.out.println("Medium");
                break;
            case 40:
                System.out.println("Large");
                break;
            default:
                System.out.println("please enter a valid size.");
        }
    }
}
