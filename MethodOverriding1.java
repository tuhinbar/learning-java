import java.util.Scanner;

class Employee1 {
    //public static int base;
    Scanner sc= new Scanner(System.in);
    long base = sc.nextLong();
    long salary()
    {
        return base;
    }
}

// Inherited class
class Manager extends Employee1 {
    long salary()
    {
        return base + 20000;
    }
}

// Inherited class
class Clerk extends Employee1 {
    long salary()
    {
        return base + 10000;
    }
}
class Main {
    static void printSalary(Employee1 e)
    {
        System.out.println(e.salary());
    }

    public static void main(String[] args)
    {

        Employee1 obj1 = new Manager();
        System.out.print("Manager's salary : ");
        printSalary(obj1);

        Employee1 obj2 = new Clerk();
        System.out.print("Clerk's salary : ");
        printSalary(obj2);
    }
}
