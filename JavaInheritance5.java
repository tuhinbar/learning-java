 class Sound {

    public void voice() {
        System.out.println("Play sound!");
    }
}
 class Flute extends Sound{
     public void voice(){
         System.out.println("Play Flute!");
     }
     public void play() {
         super.voice();
         voice();
     }
 }


class Drum extends Sound {

    public void voice() {
        System.out.println("Play drum!");
    }

    public void play() {
        super.voice();
        voice();
    }

    public static void main(String[] args) {
        Flute flute1=new Flute();
        Drum Drum1 = new Drum();
        Drum1.play();
        flute1.play();

    }
}