import java.util.Scanner;
public class ArmstrongNumber {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Insert a number:");
        int n= sc.nextInt();
        int temp=n;
        int res=0;
        while(temp>0){
            int lastDigit=temp%10;
            res=res+lastDigit*lastDigit*lastDigit;
            temp=temp/10;
        }
        if(res==n){
            System.out.println("The number is an Armstrong Number.");}
        else{
            System.out.println("The number is not an Armstrong Number.");
        }
    }
}
