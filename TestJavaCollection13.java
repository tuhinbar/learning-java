import java.util.*;
public class TestJavaCollection13 {
    public static void main(String[] args) {
        Map<String, Integer> numbers = new HashMap<>();
        numbers.put("One", 1);
        numbers.put("Two", 2);
        numbers.put("Three", 3);
        numbers.put("Four", 4);
        numbers.put("Five", 5);
        numbers.put("Three", 22);
        numbers.put("six", 0);
        System.out.println(numbers);
        if (!numbers.containsKey("Three")) {
            numbers.put("Three", 20);
        }
        numbers.putIfAbsent("six", 11);
        System.out.println(numbers);
        for (Map.Entry<String, Integer> e : numbers.entrySet()) {
            System.out.println(e);
            System.out.println(e.getKey());
            System.out.println(e.getValue());
        }
        for(String key: numbers.keySet()) {
            System.out.println(key);
        }
        for(Integer value:numbers.values()) {
            System.out.println(value);
        }
        System.out.println(numbers.containsValue(1));



        }

        }
