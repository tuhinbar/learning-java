public class LearnExceptionHandling2 {
    static void avg(){
        try {
            throw new ArithmeticException("Exception");
        }
        catch(ArithmeticException e){
            System.out.println("Exception caught.");
        }
    }

    public static void main(String[] args) {
        avg();
    }
}
