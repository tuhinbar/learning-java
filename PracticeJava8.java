class Student5{
    String name;
    int roll;
    Student5(int i,String n) {//parametrized constructor.
        roll = i;
        name = n;
    }
        void display(){
            System.out.println(roll+" "+name);
        }

    public static void main(String[] args) {
        //creating objects and passing values.
        Student5 s1=new Student5(11,"Aman");
        Student5 s2=new Student5(12,"Raman");
        //calling method to display the objects.
        s1.display();
        s2.display();
    }

    }



