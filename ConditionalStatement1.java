import java.util.Scanner;

public class ConditionalStatement1 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Please enter a age:");
        int age=sc.nextInt();
        if(age>=18){
            System.out.println("You're an adult.");
        }else{
            System.out.println("You're not an adult.");
        }
    }

}
