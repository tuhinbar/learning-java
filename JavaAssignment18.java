import java.util.Scanner;

public class JavaAssignment18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter three numbers:");
        double a = sc.nextDouble();
        double b=sc.nextDouble();
        double c=sc.nextDouble();
        System.out.println("The average of the given numbers is :"+" "+average(a,b,c));
    }

    public static double average(double a, double b, double c) {
        return (a+b+c)/3;
    }
}
