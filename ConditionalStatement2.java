import java.util.Scanner;

public class ConditionalStatement2 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        System.out.println("Please enter a number:");
        int number=sc.nextInt();
        if(number%2==0){
            System.out.println("The given number is even.");
        }else{
            System.out.println("The given number is odd.");
        }

    }
}
