//ArrayIndexOutOfBoundsException
public class LearnExceptionHandling4 {
    public static void main(String[] args) {
        try{
        int myNumbers[]={1,2,3,4,5};
        System.out.println(myNumbers[11]);
    }catch(Exception e){
            System.out.println("Exception found.");
        }
        finally {
            System.out.println("Try-catch is finished.");
        }
        }
}
